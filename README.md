# Best Sound

Webbot que adquire informações das playlists da web.

Link para a apresentação do Best Sound https://www.youtube.com/watch?v=7IuULFR4Bnk


**Desenvolvedores**

Thiago Henrique De Souza

Leonardo Gomes Tecchio

Fernanda Miyuki

[Giovana Neves](https://gitlab.com/giovana_nvs)

**Funcionalidades**

•Acessar API e elementos do sites 

•Extrair dados da playlist

•Transferir os dados para um arquivo em CSV

**Ferramentas **

•Python

•Spotipy

•CSV

•Json

