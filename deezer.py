import csv
import pandas as pd
import json
import requests
sessao = requests.session()

playlistId = '1111141961'

reqToken = sessao.post('https://www.deezer.com/ajax/gw-light.php?method=deezer.getUserData&input=3&api_version=1.0&api_token=&cid=984635453')
jsonToken = json.loads(reqToken.content.decode('utf-8'))

token = jsonToken['results']['checkForm']

reqPlaylist = sessao.post(
    'https://www.deezer.com/ajax/gw-light.php?method=playlist.getSongs&input=3&api_version=1.0&api_token={}&cid=30130050'.format(
        token
    ),
    json={
        'nb': 2000,
        'playlist_id': playlistId,
        'start': 0,
    }
)

jsonPlaylist = json.loads(reqPlaylist.content.decode('utf-8'))
#for song in jsonPlaylist['results']['data']:
    # c = json.loads(song['SNG_TITLE'])
    # a = pd.json_normalize(song['SNG_TITLE'])
    #print (song['SNG_TITLE'])
#c.to_csv('b')
df = pd.json_normalize(jsonPlaylist['results']['data']) 

df.to_csv("listdeezer.csv")