from spotipy.oauth2 import SpotifyClientCredentials
import pandas as pd
import spotipy
from pprint import pprint



sp = spotipy.Spotify(client_credentials_manager=SpotifyClientCredentials(client_id="dd473aaade904e9aa87a533a34dea032",client_secret="10bb8673fd0245e2bb4fcd67c449ee40"))

pl_id = 'spotify:playlist:37i9dQZF1DX54NB08XsyEC'
offset = 0

while True:
    response = sp.playlist_tracks(pl_id, 
                                 offset=offset,
                                 limit=100,
                                 fields=['items.track.name'])
                           
    df = pd.DataFrame(response['items'])
    df.to_csv('listspotify.csv', sep=';', encoding='UTF8',index=True)

    if len(response['items']) == 0:
        break

    need = []
    for i, item in enumerate(response['items']):
        track = item['track']
        need.append((i, track['name']))
 
    new_df = pd.DataFrame(need, columns=('Name'))
    offset = offset + len(response['items'])